<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Follower extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'follower_user_id',
        'following_user_id'
    ];

    public function userFollowing(){
    	// arguments in belongsTo (model, foreign_key, owner_key)
        return $this->belongsTo(User::class, 'following_user_id', 'id');
    }

    public function userFollower(){
    	// arguments in belongsTo (model, foreign_key, owner_key)
        return $this->belongsTo(User::class, 'follower_user_id', 'id');
    }
}
