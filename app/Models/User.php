<?php

namespace App\Models;

use Dotenv\Parser\Lexer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'last_name',
        'first_name',
        'middle_name',
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts() {
        return $this->hasMany(Post::class)
            ->orderBy('created_at', 'desc');
    }

    public function comments(){
        return $this->hasMany(Comment::class)
            ->orderBy('created_at', 'desc');
    }

    public function likes(){
        return $this->hasMany(Like::class)
            ->orderBy('created_at', 'desc');
    }

    public function followings(){
    	// arguments in hasMany (model, foreign_key, local_key)
        return $this->hasMany(Follower::class, 'follower_user_id', 'id')->inRandomOrder();
    }

    public function followers(){
    	// arguments in hasMany (model, foreign_key, local_key)
        return $this->hasMany(Follower::class, 'following_user_id', 'id')->inRandomOrder();
    }

    public function isFollowing(User $user){
        return $this->followings->contains('following_user_id', $user->id);
    }

    public function sharedPosts(){
        return $this->hasMany(SharedPost::class);
    }
}
