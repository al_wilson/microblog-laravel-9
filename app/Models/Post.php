<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class)
            ->orderBy('created_at', 'desc');
    }

    public function likes(){
        return $this->hasMany(Like::class)
            ->orderBy('created_at', 'desc');
    }

    public function likedByUser(User $user){
        return $this->likes->contains('user_id', $user->id);
    }

    public function sharedByUsers(){
        return $this->hasMany(SharedPost::class);
    }

    public function sharedByUser(User $user){
        foreach ($this->sharedByUsers as $sharedByUser) {
            if ($user->id == $sharedByUser->user_id) {
                return $sharedByUser;
                break;
            }
        }
    }
    
}
