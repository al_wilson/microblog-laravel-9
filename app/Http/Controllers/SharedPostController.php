<?php

namespace App\Http\Controllers;

use App\Http\Requests\SharedPostRequest;
use App\Models\Post;
use App\Models\SharedPost;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SharedPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SharedPostRequest $request)
    {
        if ($request->ajax()) {
            $validated = $request->validated();
            $post_user = Post::findOrFail($request['post_id'])->user;
            if (Auth::user()->isFollowing($post_user)) {
                $sharedPost = new SharedPost();
                $sharedPost->post_id = $request['post_id'];
                $sharedPost->user_id = Auth::user()->id;
                $sharedPost->content = $request['sharePost_content'];
                $sharedPost->save();

                session()->flash('success', 'Post Successfully Shared');
                return response()->json([
                    'route' => route('user.profile', Auth::user())
                ],200);
            }
            abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, SharedPost $sharedPost)
    {
        $sharedPost->post['sharedPost'] = $sharedPost;
        $post = $sharedPost->post;
        $isSharedPost = true;

        return view('pages.post.showPost', compact(
            'user',
            'post',
            'isSharedPost'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SharedPost $sharedPost)
    {
        $this->authorize('update', $sharedPost);
        $sharedPost->post->user;
        $sharedPost['new_updated_at'] = $sharedPost->updated_at->diffForHumans();
        $sharedPost->post['new_created_at'] = $sharedPost->post->created_at->diffForHumans();
        $sharedPost->post['new_updated_at'] = $sharedPost->post->updated_at->diffForHumans();
        return response()->json([
            'sharedPost' => $sharedPost
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SharedPostRequest $request, SharedPost $sharedPost)
    {
        if ($request->ajax()) {
            $validated = $request->validated();
            $this->authorize('update', $sharedPost);

            $sharedPost->content = $request['sharePost_content'];
            $sharedPost->save();

            session()->flash('success', 'Shared Post Successfully Updated');
            return response()->json([
                'route' => route('sharedPost.show', [$sharedPost->user, $sharedPost])
            ],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SharedPost $sharedPost)
    {
        $this->authorize('delete', $sharedPost);
        $sharedPost->delete();
        session()->flash('warning', 'Shared Post Successfully Deleted');

        return response()->json([
            'route' => route('user.profile', Auth::user())
        ],200);
    }
}
