<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // get users owned post
        foreach(Auth::user()->posts as $post){
            $post['new_created_at'] = $post->created_at;
        }
        // get following users post
        if (Auth::user()->followings()->count() > 0) {
            foreach (Auth::user()->followings as $following){
                foreach ($following->userFollowing->posts as $post) {
                    $post['new_created_at'] = $post->created_at;
                    Auth::user()->posts->push($post);
                }
            }
        }
        // get following users shared post
        if (Auth::user()->followings()->count() > 0) {
            foreach (Auth::user()->followings as $following){
                foreach ($following->userFollowing->sharedPosts as $sharedPost) {
                    $sharedPost->post['sharedPost'] = $sharedPost;
                    $sharedPost->post['new_created_at'] = $sharedPost->created_at;
                    Auth::user()->posts->push($sharedPost->post);
                }
            }
        }
        // get users shared post
        if(Auth::user()->sharedPosts->count() > 0) {
            foreach (Auth::user()->sharedPosts as $sharedPost) {
                $sharedPost->post['sharedPost'] = $sharedPost;
                $sharedPost->post['new_created_at'] = $sharedPost->created_at;
                Auth::user()->posts->push($sharedPost->post);
            }
        }

        $posts = Auth::user()->posts->sortByDesc('new_created_at');
        $posts = $this->paginate($posts)->withPath('');
        $user = Auth::user();
        $route = route('home');

        return view('pages.home', compact('posts', 'user', 'route'));
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
