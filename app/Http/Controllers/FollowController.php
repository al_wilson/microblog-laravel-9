<?php

namespace App\Http\Controllers;

use App\Models\Follower;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    public function follow(Request $request, User $user){
        $follower = Follower::firstOrCreate([
            'follower_user_id' => Auth::user()->id,
            'following_user_id' => $user->id
        ]);

        $followingCount = Auth::user()->followings->count() . " Following";

        if (($request['routeName'] != null) && ($request['routeName'] == 'user.followers')) {
            session()->flash('success', 'User Successfully Followed');
            return redirect()->route('user.followers', Auth::user());
        } else if (($request['url'] != null) && ($request['url'] == route('user.profile', $user))) {
            session()->flash('success', 'User Successfully Followed');
            return response()->json([
                'route' => route('user.profile', $user)
            ],200);
        }
        return response()->json([
            'status' => 'follow',
            'followingCount' => $followingCount,
            'url' => route('user.profile', Auth::user())
        ],200);
    }

    public function unfollow(Request $request, User $user){
        $follower = Follower::where([
            ['follower_user_id', Auth::user()->id],
            ['following_user_id', $user->id]
        ])->first();
        $follower->delete();

        $followingCount = Auth::user()->followings->count() . " Following";

        if (($request['routeName'] != null) && ($request['routeName'] == 'user.followers')) {
            session()->flash('warning', 'User Successfully Unfollowed');
            return redirect()->route('user.followers', Auth::user());
        } else if (($request['url'] != null) && ($request['url'] == route('user.profile', $user))) {
            session()->flash('warning', 'User Successfully Unfollowed');
            return response()->json([
                'route' => route('user.profile', $user)
            ],200);
        }
        return response()->json([
            'status' => 'unfollow',
            'followingCount' => $followingCount,
            'url' => route('user.profile', Auth::user())
        ],200);
    }

    public function index(User $user){
        return view('pages.follow.index')
            ->with('user', $user);
    }
}
