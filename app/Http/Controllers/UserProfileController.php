<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Http\Requests\UserRequest;
use App\Models\Post;
use App\Models\User;
use App\Rules\MatchCurrentPassword;
use Dotenv\Repository\RepositoryInterface;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class UserProfileController extends Controller
{
    public function profile(User $user) {
        // get users owned post
        foreach($user->posts as $post){
            $post['new_created_at'] = $post->created_at;
        }

        // get users shared posts
        if($user->sharedPosts->count() > 0) {
            foreach ($user->sharedPosts as $sharedPost) {
                $sharedPost->post['sharedPost'] = $sharedPost;
                $sharedPost->post['new_created_at'] = $sharedPost->created_at;
                $user->posts->push($sharedPost->post);
            }
        }

        $posts = $user->posts->sortByDesc('new_created_at');
        $posts = $this->paginate($posts)->withPath('');

        return view('pages.user.profile')
            ->with('user', $user)
            ->with('posts', $posts)
            ->with('followers', $user->followers)
            ->with('followings', $user->followings)
            ->with('route', route('user.profile', $user));
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function update(UserRequest $request, User $user) {
        if ($request->ajax()) {
            $validated = $request->validated();
            $this->authorize('update', $user);

            $user->last_name = $request['lastName'];
            $user->first_name = $request['firstName'];
            $user->middle_name = $request['middleName'];
            $user->username = $request['userName'];
            if ($request['newPassword'] != null) {
                $user->password = Hash::make($request['newPassword']);
            }
            if ($request['imagePath']) {
                if ($request['imagePath'] != $user->image) {
                    // check if user has image
                    if (($user->image != null) && $user->image != '/uploads/user_images/default-user-image.png') {
                        // delete the old image
                        if (file_exists(public_path($user->image))) {
                            unlink(public_path($user->image));
                        } else {
                            return response()->json([
                                'error' => 'Something went wrong!'
                            ], 500);
                        }
                    }
                    $imageName = explode('/', $request['imagePath']);
                    $imageName = end($imageName);
                    // create user image file directory
                    $user_image_path = public_path('/uploads/user_images/');
                    if (!file_exists($user_image_path)) {
                        mkdir($user_image_path, 0777, true);
                    }
                    // move image file from temp directory to new file directory
                    if (file_exists(public_path() . $request['imagePath'])) {
                        File::move(
                            public_path() . $request['imagePath'],
                            $user_image_path . $imageName
                        );
                    }
                    // save the file directory to database
                    $user->image = '/uploads/user_images/' . $imageName;
                }
            }
            $user->save();

            session()->flash('success', 'User Info Successfully Updated');
            return response()->json([
                'route' => route('user.profile', $user)
            ],200);
        }
    }

    public function uploadUserImage(ImageRequest $request){
        if ($request->ajax()) {
            $validated = $request->validated();

            if ($request->file('image')) {
                // create temp file directory for images
                $temp_path = public_path('/tmp/uploads/');
                if (!file_exists($temp_path)) {
                    mkdir($temp_path, 0777, true);
                }
                // rename the file
                $newImageName = time() . '.' . $request->file('image')->getClientOriginalExtension();
                // move file to temp file directory
                $request->file('image')->move($temp_path, $newImageName);

                $newImageFilePath = '/tmp/uploads/' . $newImageName;   

                return response()->json([
                    'filePath' => $newImageFilePath,
                    'fileName' => $newImageName
                ]);
            }
        }
    }

    public function removeUserImage(Request $request) {
        if ($request->ajax()) {
            if (file_exists(public_path($request['filePath']))) {
                if (unlink(public_path($request['filePath']))) {
                    return response()->json([
                        'success'=> '1'
                    ], 200);
                } else {
                    return response()->json([
                        'error' => 'Something went wrong!'
                    ], 500);
                }
            }
        }
    }
}
