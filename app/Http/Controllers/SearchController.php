<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\ComparisonMethodDoesNotDeclareBoolReturnTypeException;

class SearchController extends Controller
{
    public function searchUser(Request $request){
        $users = User::where('username', 'like', '%'. $request['searchUsername'] .'%')
            ->orWhere('last_name', 'like', '%'. $request['searchUsername'] .'%')
            ->orWhere('first_name', 'like', '%'. $request['searchUsername'] .'%')
            ->paginate(5)->withPath('search?searchUsername=' . $request['searchUsername']);

        return view('pages.search.index')
            ->with('users', $users)
            ->with('searchUsername', $request['searchUsername']);
    }
}
