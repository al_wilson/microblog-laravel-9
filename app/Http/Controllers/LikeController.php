<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Dotenv\Repository\RepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Support\Str;

class LikeController extends Controller
{
    public function like(Request $request, Post $post) {
        if ($request->ajax()) {
            $like = Like::firstOrCreate([
                'user_id' => Auth::user()->id,
                'post_id' => $post->id
            ]);

            $likesCount = $post->likes->count() . " " . Str::plural('Like', $post->likes->count());
            
            return response()->json([
                'status' => 'like',
                'likesCount' => $likesCount
            ],200);
        }
    }

    public function unlike(Request $request, Post $post){
        if ($request->ajax()) {
            $like = Like::where([
                ['user_id', Auth::user()->id],
                ['post_id', $post->id]
            ])->first();
            $like->delete();

            $likesCount = $post->likes->count() . " " . Str::plural('Like', $post->likes->count());

            return response()->json([
                'status' => 'unlike',
                'likesCount' => $likesCount
            ],200);
        }
    }
}
