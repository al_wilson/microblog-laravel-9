<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use Illuminate\Http\Client\ResponseSequence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        if ($request->ajax()) {
            $validated = $request->validated();

            $comment = new Comment();
            $comment->user_id = Auth::user()->id;
            $comment->post_id = $request['post_id'];
            $comment->comment = $request['comment'];
            $comment->save();

            session()->flash('success', 'Comment Successfully Created');
            return response()->json([
                'route' => route('post.show', [$comment->post->user, $comment->post])
            ],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        if ($request->ajax()) {
            $validated = $request->validated();
            $this->authorize('update', $comment);

            $comment->comment = $request['comment'];
            $comment->save();

            session()->flash('success', 'Comment Successfully Updated');
            return response()->json([
                'route' => route('post.show', [$comment->post->user, $comment->post])
            ],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);
        
        $comment->delete();
        session()->flash('warning', 'Comment Successfully Deleted');
        return response()->json([
            'route' => route('post.show', [$comment->post->user, $comment->post])
        ],200);
        
    }
}
