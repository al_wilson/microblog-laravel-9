<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\SharedPost;
use App\Models\User;
use File;
use Illuminate\Database\PDO\PostgresDriver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Ui\Presets\React;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Route;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request) {
        if ($request->ajax()) {
            $validated = $request->validated();
            
            $post = new Post();
            $post->user_id = Auth::user()->id;
            $post->content = $request['content'];
            if ($request['imagePath']) {
                $imageName = explode('/', $request['imagePath']);
                $imageName = end($imageName);
                // create posts image file directory
                $post_image_path = public_path('/uploads/post_images/');
                // move image file from temp directory to new file directory
                if (file_exists(public_path() . $request['imagePath'])) {
                    File::move(
                        public_path() . $request['imagePath'],
                        $post_image_path . $imageName
                    );
                }
                // save the file directory to database
                $post->image = '/uploads/post_images/' . $imageName;
            }
            $post->save();

            session()->flash('success', 'Post Successfully Created');
            return response()->json([
                'route' => route('post.show', [Auth::user(), $post])
            ],200);
        }
    }

    public function uploadPostImage(ImageRequest $request){
        if ($request->ajax()) {
            $validated = $request->validated();

            if ($request->file('image')) {
                // create temp file directory for images
                $temp_path = public_path('/tmp/uploads/');
                if (!file_exists($temp_path)) {
                    mkdir($temp_path, 0777, true);
                }
                // rename the file
                $newImageName = time() . '.' . $request->file('image')->getClientOriginalExtension();
                // move file to temp file directory
                $request->file('image')->move($temp_path, $newImageName);

                $newImageFilePath = '/tmp/uploads/' . $newImageName;   

                return response()->json([
                    'filePath' => $newImageFilePath,
                    'fileName' => $newImageName
                ]);
            }
        }
    }

    public function removePostImage(Request $request) {
        if ($request->ajax()) {
            if (file_exists(public_path($request['filePath']))) {
                if (unlink(public_path($request['filePath']))) {
                    return response()->json([
                        'success'=> '1'
                    ], 200);
                } else {
                    return response()->json([
                        'error' => 'Something went wrong!'
                    ], 500);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Post $post)
    {
        $this->authorize('view', $post);
        $post->comments = $this->paginate($post->comments)->withPath('');
        return view('pages.post.showPost', compact('user', 'post'));
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $this->authorize('edit', $post);
        $post['user'] = $post->user;
        $post['new_created_at'] = $post->created_at->diffForHumans();
        $post['new_updated_at'] = $post->updated_at->diffForHumans();

        return response()->json([
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        if ($request->ajax()) {
            $validated = $request->validated();
            $this->authorize('update', $post);

            $post->content = $request['content'];
            if ($request['imagePath']) {
                if ($request['imagePath'] != $post->image) {
                    // check if post has image
                    if ($post->image != null) {
                        // delete the old image
                        if (file_exists(public_path($post->image))) {
                            unlink(public_path($post->image));
                        } else {
                            return response()->json([
                                'error' => 'Something went wrong!'
                            ], 500);
                        }
                    }
                    $imageName = explode('/', $request['imagePath']);
                    $imageName = end($imageName);
                    // create posts image file directory
                    $post_image_path = public_path('/uploads/post_images/');
                    if (!file_exists($post_image_path)) {
                        mkdir($post_image_path, 0777, true);
                    }
                    // move image file from temp directory to new file directory
                    if (file_exists(public_path() . $request['imagePath'])) {
                        File::move(
                            public_path() . $request['imagePath'],
                            $post_image_path . $imageName
                        );
                    }
                    // save the file directory to database
                    $post->image = '/uploads/post_images/' . $imageName;
                }
            }
            $post->save();

            session()->flash('success', 'Post Successfully Updated');
            return response()->json([
                'route' => route('post.show', [Auth::user(), $post])
            ],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);

        if ($post->image != null) {
            if(file_exists(public_path($post->image))) {
                unlink(public_path($post->image));
            }
        }
        $post->delete();

        session()->flash('warning', 'Post Successfully Deleted');
        return response()->json([
            'route' => route('user.profile', Auth::user())
        ],200);
    }
}
