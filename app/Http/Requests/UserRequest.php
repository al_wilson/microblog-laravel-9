<?php

namespace App\Http\Requests;

use App\Rules\MatchCurrentPassword;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastName' => [
                'required',
                'string',
                'max:255'
            ],
            'firstName' => [
                'required',
                'string',
                'max:255'
            ],
            'userName' => [
                'required',
                'string',
                'max:255',
                'alpha_num',
                Rule::unique('users')->ignore($this->user)
            ],
            'currentPassword' => [
                'nullable',
                'required_with:newPassword',
                new MatchCurrentPassword
            ],
            'newPassword' => [
                'bail',
                'required_with:currentPassword',
                'nullable',
                'string',
                'min:8'
            ],
            'confirmPassword' => [
                'same:newPassword',
                'required_with:newPassword',
            ]
        ];
    }
}
