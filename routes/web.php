<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SharedPostController;
use App\Http\Controllers\UserProfileController;
use App\Models\SharedPost;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// add middleware for XSS prevention
Route::middleware(['XSSPrevention'])->group(function () {
    Route::get('/', function () {
        return redirect('login');
    });
    
    Auth::routes(['verify' => true]);
    
    Route::middleware(['auth', 'verified'])->group(function(){
        Route::get('/home', [HomeController::class, 'index'])->name('home');
        // route for post
        Route::resource('post', PostController::class)->except(['show']);
        Route::prefix('post')->group(function () {
            Route::post('/uploadPostImage', [PostController::class, 'uploadPostImage'])->name('uploadPostImage');
            Route::post('/removePostImage', [PostController::class, 'removePostImage'])->name('removePostImage');
            // route for like/unlike
            Route::post('/{post}/like', [LikeController::class, 'like'])->name('post.like');
            Route::post('/{post}/unlike', [LikeController::class, 'unlike'])->name('post.unlike');
        });
        // route for sharing post
        Route::resource('sharedPost', SharedPostController::class)->except(['show']);
        // route for profile page
        Route::prefix('profile/{user:username}')->group(function () {
            // user profile
            Route::get('', [UserProfileController::class, 'profile'])->name('user.profile');
            Route::post('', [UserProfileController::class, 'update'])->name('user.update');
            // user post and shared post view
            Route::get('/Post/{post}', [PostController::class, 'show'])->name('post.show');
            Route::get('/sharedPost/{sharedPost}', [SharedPostController::class, 'show'])->name('sharedPost.show');
            // user image functions
            Route::post('/uploadUserImage', [UserProfileController::class, 'uploadUserImage'])->name('user.uploadImage');
            Route::post('/removeUserImage', [UserProfileController::class, 'removeUserImage'])->name('user.removeImage');
            // route for follow and unfollow user
            Route::post('/follow', [FollowController::class, 'follow'])->name('user.follow');
            Route::post('/unfollow', [FollowController::class, 'unfollow'])->name('user.unfollow');
            // route for list view of follower and following
            Route::get('/followers', [FollowController::class, 'index'])->name('user.followers');
        });
        // route for comment
        Route::resource('comment', CommentController::class);
        // route for searching user
        Route::get('search', [SearchController::class, 'searchUser'])->name('search.users');
    });
});