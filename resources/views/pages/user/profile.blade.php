@extends('layouts.app')

@section('content')

@include('partials.flashMessages')
@include('pages.user.info')

<!-- for logedIn user profile -->
@if ($user->id == Auth::user()->id)
    <br>
    @include('pages.post.createPost')
    @include('pages.post.editPost')
    @include('pages.post.deletePost')
    @include('pages.user.edit')
    @include('pages.post.indexPost')
@endif

<!-- for other user profile -->
@if (Auth::user()->isFollowing($user))
    @include('pages.post.indexPost')
@endif

@include('pages.comment.delete')
@include('pages.post.sharePost')

@endsection

@section('js')

<!-- for updating user info -->
<script src="{{mix('js/user/editUserInfo.js')}}"></script>

<!-- for logedIn user profile Js File-->
@if ($user->id == Auth::user()->id)
    <!-- js file for post -->
    <script src="{{mix('js/post/createPost.js')}}"></script>
    <script src="{{mix('js/post/editPost.js')}}"></script>
    <script src="{{mix('js/post/deletePost.js')}}"></script>
@endif
<script src="{{mix('js/post/sharePost.js')}}"></script>
<script src="{{mix('js/post/editSharedPost.js')}}"></script>
<script src="{{mix('js/post/deleteSharedPost.js')}}"></script>
<!-- js file for comment -->
<script src="{{mix('js/comment/create.js')}}"></script>
<script src="{{mix('js/comment/edit.js')}}"></script>
<script src="{{mix('js/comment/delete.js')}}"></script>
<!-- js file for like and unlike -->
<script src="{{mix('js/like.js')}}"></script>
<!-- js file for follow and unfollow -->
<script src="{{mix('js/follow.js')}}"></script>

@endsection