<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-3">
                                <div class="user-profile-img-div">
                                    <img class="img-fluid img-thumbnail user-profile-img"
                                    src="{{$user->image ?? asset('uploads/user_images/default-user-image.png')}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <h1>
                                                {{$user->first_name}}
                                                {{$user->middle_name}}
                                                {{$user->last_name}}
                                            </h1>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align: left">
                                        <div class="col-3">
                                            <p>
                                                {{$user->followers->count()}}
                                                {{Str::plural('Follower', $user->followers->count())}}
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <p id="userFollowingCount">
                                                {{$user->followings->count()}}
                                                Following
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <p>
                                                {{$user->posts->count()}}
                                                {{Str::plural('Post', $user->posts->count())}}
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            @if ($user->id == Auth::user()->id)
                                                <button type="button"
                                                    class="btn btn-light"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#editUserProfileModal">
                                                    Edit Profile
                                                </button>                                                
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            @if ($user->id != Auth::user()->id)
                                                <button type="submit"
                                                    class="btn btn-outline-info"
                                                    style="width: 150px"
                                                    onclick="{{Auth::user()->isFollowing($user) ? 'unfollow' : 'follow'}}('{{$user->username}}')">
                                                    {{Auth::user()->isFollowing($user) ? 'Unfollow' : 'Follow'}}
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
