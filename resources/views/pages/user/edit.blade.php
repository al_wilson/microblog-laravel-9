<!-- Edit User Profile Modal -->
<div class="modal fade"
    id="editUserProfileModal"
    tabindex="-1"
    aria-labelledby="editUserProfileModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Profile</h5>
                <button type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close">
                </button>
            </div>

            <div class="modal-body">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="user-profile-img-div">
                                        <img class="img-fluid user-profile-img"
                                        id="userImagePrev"
                                        src="{{$user->image ?? asset('uploads/user_images/default-user-image.png')}}"
                                        alt="">
                                    </div>

                                    <span id="userImagePrevErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>

                                    <div style="text-align: center"
                                        id="removeUserImageDiv">
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <form method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <input type="file"
                                    id="userImage"
                                    name="userImage"
                                    class="form-control">

                                    <span id="userImageErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                <br>
                                <div style="text-align: center">
                                    <button type="button"
                                        id="uploadUserImageBtn"
                                        onclick="uploadUserImage()"
                                        class="btn btn-outline-secondary">
                                        Upload
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col-8">
                            <div style="text-align: left">
                                <input type="hidden"
                                    name="originalUserImagePath"
                                    id="originalUserImagePath"
                                    value="{{$user->image}}">
                                <div>
                                    <label for="lastName">
                                        Last Name
                                    </label>
                                    <input type="text"
                                        name="lastName"
                                        id="lastName"
                                        class="form-control"
                                        value="{{$user->last_name}}">
                                    
                                    <span id="lastNameErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <br>
                                <div>
                                    <label for="firstName">
                                        First Name
                                    </label>
                                    <input type="text"
                                        name="firstName"
                                        id="firstName"
                                        class="form-control"
                                        value="{{$user->first_name}}">
                                    
                                    <span id="firstNameErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <br>
                                <div>
                                    <label for="middleName">
                                        Middle Name
                                    </label>
                                    <input type="text"
                                        name="middleName"
                                        id="middleName"
                                        class="form-control"
                                        value="{{$user->middle_name}}">

                                    <span id="middleNameErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <br>
                                <div>
                                    <label for="userName">
                                        Username
                                    </label>
                                    <input type="text"
                                        name="userName"
                                        id="userName"
                                        class="form-control"
                                        value="{{$user->username}}">

                                    <span id="userNameErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <br>
                                <div>
                                    <label for="email">
                                        E-mail
                                    </label>
                                    <input type="email"
                                        name="email"
                                        id="email"
                                        class="form-control"
                                        value="{{$user->email}}"
                                        disabled>

                                    <span id="emailErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <br>
                                <div>
                                    <label for="currentPassword">
                                        Current Password
                                    </label>
                                    <input type="password"
                                        name="currentPassword"
                                        id="currentPassword"
                                        class="form-control">

                                    <span id="currentPasswordErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <br>
                                <div>
                                    <label for="newPassword">
                                        New Password
                                    </label>
                                    <input type="password"
                                        name="newPassword"
                                        id="newPassword"
                                        class="form-control">

                                    <span id="newPasswordErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <br>
                                <div>
                                    <label for="confirmPassword">
                                        Confirm Password
                                    </label>
                                    <input type="password"
                                        name="confirmPassword"
                                        id="confirmPassword"
                                        class="form-control">

                                    <span id="confirmPasswordErrorSpan"
                                        class="invalid-feedback"
                                        role="alert">
                                    </span>
                                </div>
                                <div>
                                    <a href="{{route('password.request')}}">Forgot your password?</a>
                                </div>
                                <div id="updateUserProfileBtnDiv">
                                    <button type="button"
                                    id="updateUserProfileBtn"
                                    class="btn btn-success"
                                    onclick="updateUserProfile()">
                                    Save
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
