<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p>Followers</p>
                    <hr>
                    <div class="container">
                        @if ($user->followers->count() > 0)
                            @foreach ($user->followers as $key => $follower)
                                @if ($key == 3)
                                    @break
                                @endif
                                <br>
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <img class="img-fluid"
                                            src="{{$follower->userFollower->image ?? asset('uploads/user_images/default-user-image.png')}}"
                                            style="width: 100%; border-radius: 50%">
                                    </div>
                                    <div class="col-6">
                                        <a href="{{route('user.profile', $follower->userFollower)}}">
                                            {{$follower->userFollower->first_name}}
                                            {{$follower->userFollower->last_name}}
                                        </a>
                                    </div>
                                    <div class="col-3">
                                        @if (Auth::user()->id != $follower->userFollower->id)
                                            <button type="submit"
                                                class="btn btn-outline-info"
                                                id="followerBtn_{{$follower->userFollower->username}}"
                                                onclick="{{Auth::user()->isFollowing($follower->userFollower) ? 'unfollow' : 'follow'}}('{{$follower->userFollower->username}}')">
                                                {{Auth::user()->isFollowing($follower->userFollower) ? 'Unfollow' : 'Follow'}}
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col">
                    <p>Following</p>
                    <hr>
                    <div class="container">
                        @if ($user->followings->count() > 0)
                            @foreach ($user->followings as $key => $following)
                                @if ($key == 3)
                                    @break
                                @endif
                                <br>
                                <div class="row align-items-center">
                                    <div class="col-3">
                                        <img class="img-fluid"
                                            src="{{$following->userFollowing->image ?? asset('uploads/user_images/default-user-image.png')}}"
                                            style="width: 100%; border-radius: 50%">
                                    </div>
                                    <div class="col-6">
                                        <a href="{{route('user.profile', $following->userFollowing)}}">
                                            {{$following->userFollowing->first_name}}
                                            {{$following->userFollowing->last_name}}
                                        </a>
                                    </div>
                                    <div class="col-3">
                                        @if (Auth::user()->id != $following->userFollowing->id)
                                            <button type="submit"
                                                class="btn btn-outline-info"
                                                id="followingBtn_{{$following->userFollowing->username}}"
                                                onclick="{{Auth::user()->isFollowing($following->userFollowing) ? 'unfollow' : 'follow'}}('{{$following->userFollowing->username}}')">
                                                {{Auth::user()->isFollowing($following->userFollowing) ? 'Unfollow' : 'Follow'}}
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="row"
                style="text-align: right">
                <div class="col">
                    <br>
                    <a href="{{route('user.followers', $user)}}">
                        View all
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>