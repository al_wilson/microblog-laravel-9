@extends('layouts.app')

@section('content')

@include('partials.flashMessages')
@include('pages.user.info')

<br>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-6">
                                <h2>Followers</h2>
                                @if ($user->followers->count() > 0)
                                    @foreach ($user->followers as $key => $follower)
                                        @if ($key == 3)
                                            @break
                                        @endif
                                        <br>
                                        <div class="row align-items-center">
                                            <div class="col-2">
                                                <img class="img-fluid"
                                                    src="{{$follower->userFollower->image ?? asset('uploads/user_images/default-user-image.png')}}"
                                                    style="width: 100%; border-radius: 50%">
                                            </div>
                                            <div class="col-6">
                                                <a href="{{route('user.profile', $follower->userFollower)}}">
                                                    {{$follower->userFollower->first_name}}
                                                    {{$follower->userFollower->last_name}}
                                                </a>
                                                <br>
                                                <p>
                                                    {{$follower->userFollower->followers->count()}}
                                                    {{Str::plural('Follower', $follower->userFollower->followers->count())}}
                                                    &nbsp
                                                    {{$follower->userFollower->followings->count()}}
                                                    Following
                                                    &nbsp
                                                    {{$follower->userFollower->posts->count()}}
                                                    {{Str::plural('Post', $follower->userFollower->posts->count())}}
                                                </p>
                                            </div>
                                            <div class="col-3">
                                                @if (Auth::user()->id != $follower->userFollower->id)
                                                    @if (Auth::user()->isFollowing($follower->userFollower))
                                                        <form action="{{route('user.unfollow', $follower->userFollower)}}"
                                                            method="POST">
                                                            @csrf
                                                            <input type="hidden"
                                                                id="routeName"
                                                                name="routeName"
                                                                value="{{Request::route()->getName()}}">
                                                            <button type="submit"
                                                                class="btn btn-outline-info"
                                                                onclick="this.form.submit();
                                                                this.disabled=true">
                                                                Unfollow
                                                            </button>
                                                        </form>
                                                    @else
                                                        <form action="{{route('user.follow', $follower->userFollower)}}"
                                                            method="POST">
                                                            @csrf
                                                            <input type="hidden"
                                                                id="routeName"
                                                                name="routeName"
                                                                value="{{Request::route()->getName()}}">
                                                            <button type="submit"
                                                                class="btn btn-outline-info"
                                                                onclick="this.form.submit();
                                                                this.disabled=true">
                                                                Follow
                                                            </button>
                                                        </form>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col">
                                <h2>Following</h2>
                                @if ($user->followings->count() > 0)
                                    @foreach ($user->followings as $key => $following)
                                        @if ($key == 3)
                                            @break
                                        @endif
                                        <br>
                                        <div class="row align-items-center">
                                            <div class="col-2">
                                                <img class="img-fluid"
                                                    src="{{$following->userFollowing->image ?? asset('uploads/user_images/default-user-image.png')}}"
                                                    style="width: 100%; border-radius: 50%">
                                            </div>
                                            <div class="col-6">
                                                <a href="{{route('user.profile', $following->userFollowing)}}">
                                                    {{$following->userFollowing->first_name}}
                                                    {{$following->userFollowing->last_name}}
                                                </a>
                                                <br>
                                                <p>
                                                    {{$following->userFollowing->followers->count()}}
                                                    {{Str::plural('Follower', $following->userFollowing->followers->count())}}
                                                    &nbsp
                                                    {{$following->userFollowing->followings->count()}}
                                                    Following
                                                    &nbsp
                                                    {{$following->userFollowing->posts->count()}}
                                                    {{Str::plural('Post', $following->userFollowing->posts->count())}}
                                                </p>
                                            </div>
                                            <div class="col-3">
                                                @if (Auth::user()->id != $following->userFollowing->id)
                                                    @if (Auth::user()->isFollowing($following->userFollowing))
                                                        <form action="{{route('user.unfollow', $following->userFollowing)}}"
                                                            method="POST">
                                                            @csrf
                                                            <input type="hidden"
                                                                id="routeName"
                                                                name="routeName"
                                                                value="{{Request::route()->getName()}}">
                                                            <button type="submit"
                                                                class="btn btn-outline-info"
                                                                onclick="this.form.submit();
                                                                this.disabled=true">
                                                                Unfollow
                                                            </button>
                                                        </form>
                                                    @else
                                                        <form action="{{route('user.follow', $following->userFollowing)}}"
                                                            method="POST">
                                                            @csrf
                                                            <input type="hidden"
                                                                id="routeName"
                                                                name="routeName"
                                                                value="{{Request::route()->getName()}}">
                                                            <button type="submit"
                                                                class="btn btn-outline-info"
                                                                onclick="this.form.submit();
                                                                this.disabled=true">
                                                                Follow
                                                            </button>
                                                        </form>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
