@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <h1>You Search for "{{$searchUsername}}"</h1>
    </div>
    @if ($users->count() > 0)
        @foreach ($users as $user)
            <br>
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-1">
                                    <img class="img-fluid"
                                        src="{{$user->image ?? asset('uploads/user_images/default-user-image.png')}}"
                                        style="width: 100%; border-radius: 50%">
                                </div>
                                <div class="col">
                                    <a href="{{route('user.profile', $user)}}">
                                        {{$user->first_name}}
                                        {{$user->middle_name}}
                                        {{$user->last_name}}
                                    </a>
                                    <br>
                                    <p>
                                        {{$user->followers->count()}}
                                        {{Str::plural('Follower', $user->followers->count())}}
                                        &nbsp
                                        {{$user->followings->count()}}
                                        Following
                                        &nbsp
                                        {{$user->posts->count()}}
                                        {{Str::plural('Post', $user->posts->count())}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <br>
        {{ $users->links() }}
    @endif
</div>

@endsection
