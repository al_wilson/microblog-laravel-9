<!-- Share Post Modal -->
<div class="modal fade"
    id="sharePostModal"
    tabindex="-1"
    aria-labelledby="sharePostModal"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Share Post</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-2">
                            <input type="hidden"
                                id="sharedPost_id"
                                name="sharedPost_id">
                            <div class="user-profile-img-div">
                                <img class="img-fluid img-thumbnail user-profile-img"
                                src="{{Auth::user()->image ?? asset('uploads/user_images/default-user-image.png')}}">
                            </div>
                        </div>
                        <div class="col">
                            <textarea class="form-control"
                                name="sharePost_content"
                                id="sharePost_content"
                                rows="4"
                                placeholder="Whats on your mind {{Auth::user()->username}}"></textarea>

                            <span id="sharePost_contentErrorSpan"
                                class="invalid-feedback"
                                role="alert">
                            </span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div id="postToShareCardBody" class="card-body">
                                    <div id="postToShareContainer"
                                        class="container">
                                        <input type="hidden"
                                            name="sharePostId"
                                            id="sharePostId">
                                        <div class="row align-items-center">
                                            <div class="col-2">
                                                <img class="img-fluid img-thumbnail"
                                                    id="sharePostUserImage"
                                                    style="width: 100%; border-radius: 50%">
                                            </div>
                                            <div class="col">
                                                <div id="sharePostUsername">
                                                </div>
                                                <span class="text-muted"
                                                    id="sharePostTimestamp"
                                                    style="font-size: .7rem">
                                                </span>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col">
                                                <div id="sharePostContent">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col"
                                                style="text-align: center">
                                                <img class="img-fluid" 
                                                    id="sharePostImage"
                                                    style="height: 200px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div id="sharePostBtnDiv"
                    style="text-align: right">
                    <button type="submit"
                        id="sharePostBtn"
                        onclick="sharePost()"
                        class="btn btn-success">
                        Share
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
