@extends('layouts.app')

@section('content')

@include('partials.flashMessages')
@include('pages.user.info')
@if ($post->user->id == Auth::user()->id)
    @include('pages.post.editPost')
    @include('pages.post.deletePost')
@endif

<br>
<div class="container">
    <div class="row">
        <div class="col">
            @if ($post->sharedByUser($user))
                @include('pages.post.sharedPostThread')
            @else
                @include('pages.post.postThread')
            @endif
        </div>
    </div>
</div>

@include('pages.comment.delete')
@include('pages.post.deletePost')
@include('pages.post.sharePost')

@endsection

@section('js')
    <script>
        $('#commentBody_{{$post->id}}').attr('class', 'collapse show');
    </script>
    @if ($post->user->id == Auth::user()->id)
        <script src="{{mix('js/post/editPost.js')}}"></script>
        <script src="{{mix('js/post/deletePost.js')}}"></script>
    @endif
    <script src="{{mix('js/post/sharePost.js')}}"></script>
    <script src="{{mix('js/post/editSharedPost.js')}}"></script>
    <script src="{{mix('js/post/deleteSharedPost.js')}}"></script>
    <!-- js file for comment -->
    <script src="{{mix('js/comment/create.js')}}"></script>
    <script src="{{mix('js/comment/edit.js')}}"></script>
    <script src="{{mix('js/comment/delete.js')}}"></script>
    <!-- js file for like and unlike -->
    <script src="{{mix('js/like.js')}}"></script>
@endsection