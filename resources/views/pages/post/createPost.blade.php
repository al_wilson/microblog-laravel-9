<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-md-1">
                                <div class="user-profile-img-div">
                                    <img class="img-fluid img-thumbnail user-profile-img"
                                        src="{{$user->image ?? asset('uploads/user_images/default-user-image.png')}}">
                                </div>
                            </div>
                            <div class="col-md">
                                <button type="button"
                                    class="btn btn-outline-secondary"
                                    style="text-align: left; width: 100%"
                                    data-bs-toggle="modal"
                                    data-bs-target="#createPostModal">
                                    What's on you're mind {{Auth::user()->username}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Create Post Modal -->
<div class="modal fade"
    id="createPostModal"
    tabindex="-1"
    aria-labelledby="createPostModal"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Post</h5>
                <button type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close">
                </button>
            </div>

            <div class="modal-body">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-md">
                            <textarea name="content" 
                                id="content"
                                class="form-control"
                                style="width: 100%; height: 200px"
                                placeholder="What's on you're mind {{Auth::user()->username}}"></textarea>

                            <span id="contentErrorSpan"
                                class="invalid-feedback"
                                role="alert">
                            </span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST"
                                enctype="multipart/form-data">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-9 col-md-9">
                                            <input class="form-control"
                                                type="file"
                                                name="image"
                                                id="image">

                                            <span id="imageErrorSpan"
                                                class="invalid-feedback"
                                                role="alert">
                                            </span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <button type="button"
                                                id="uploadImageBtn"
                                                style="width: 100px"
                                                onclick="uploadPostImage()"
                                                class="btn btn-outline-secondary">
                                                Upload
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md"
                            style="text-align: center">
                            <img class="img-fluid"
                                id="imagePrev"
                                style="height: 200px;"
                                src=""
                                alt="">

                            <span id="imagePrevErrorSpan"
                                class="invalid-feedback"
                                role="alert">
                            </span>

                            <br>
                            <br>
                            <button id="removeImageBtn"
                                hidden
                                onclick="removeImage()" 
                                class="btn btn-danger">
                                Remove
                            </button>
                        </div>
                    </div>
                </div>
                <br>
                <div style="text-align: right">
                    <button type="submit"
                        id="storePostBtn"
                        class="btn btn-success"
                        onclick="storePost()">
                        Post
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
