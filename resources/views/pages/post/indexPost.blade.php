<div class="container">
    <div class="row justify-content-center">
        <div class="col-8">
            @if (count($posts) > 0)
                @foreach ($posts as $post)
                    <br>
                    @if ($post->sharedPost)
                        @include('pages.post.sharedPostThread')
                    @else
                        @include('pages.post.postThread')
                    @endif
                @endforeach
                <br>
                {{ $posts->links() }}
            @endif
        </div>
        <div class="col">
            <br>
            @include('pages.follow.followPanel')
        </div>
    </div>
</div>