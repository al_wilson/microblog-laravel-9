<div class="card">
    <div class="card-body">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-2">
                    <div class="user-profile-img-div">
                        <img class="img-fluid img-thumbnail user-profile-img"
                            src="{{$post->sharedPost->user->image ?? asset('uploads/user_images/default-user-image.png')}}">
                    </div>
                </div>
                <div class="col-md">
                    <a href="{{route('user.profile', $user)}}">
                        {{$post->sharedPost->user->first_name}}
                        {{$post->sharedPost->user->middle_name}}
                        {{$post->sharedPost->user->last_name}}
                    </a>
                    &nbsp;
                    <label class="text-muted">Shared <i class="fa-solid fa-share"></i></label>
                    <a href="{{route('user.profile', $post->user)}}">
                        {{$post->user->first_name}}
                        {{$post->user->middle_name}}
                        {{$post->user->last_name}}
                    </a>
                    <label class="text-muted">Post/s</label>
                    @if ($post->sharedPost->created_at != $post->sharedPost->updated_at)
                        <span class="text-muted"
                            style="font-size: .7rem">
                            Edited
                            {{$post->sharedPost->updated_at->diffForHumans()}}
                        </span>
                        <i class="fa-solid fa-pen"></i>
                    @endif
                    <br>
                    <span class="text-muted"
                        style="font-size: .7rem">
                        {{$post->sharedPost->created_at->diffForHumans()}}
                    </span>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col col-md">
                    <p>
                        {{$post->sharedPost->content}}
                        <a style="float: right"
                            href="{{route('sharedPost.show', [$post->sharedPost->user, $post->sharedPost->id])}}">
                            See post
                            <i class="fa-solid fa-angles-right"></i>
                        </a>
                    </p>
                </div>
            </div>
            <div class="row" style="text-align: right">
                <div class="col offset-9">
                    @if ($post->sharedPost->user->id == Auth::user()->id)
                        <button type="button"
                            class="btn btn-light"
                            onclick="sharedPostModal('{{$post->sharedPost->id}}')">
                            Edit
                        </button>
                        <button type="button"
                            class="btn btn-light"
                            onclick="deleteSharedPostModal({{$post->sharedPost->id}})">
                            Delete
                        </button>
                    @endif
                </div>
            </div>
            <br>
        </div>
        @if ($post->deleted_at == null)
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="user-profile-img-div">
                                    <img class="img-fluid img-thumbnail user-profile-img"
                                        src="{{$post->user->image ?? asset('uploads/user_images/default-user-image.png')}}">
                                </div>
                                
                            </div>
                            <div class="col-md">
                                <a href="{{route('user.profile', $post->user)}}">
                                    {{$post->user->first_name}}
                                    {{$post->user->middle_name}}
                                    {{$post->user->last_name}}
                                </a>
                                @if ($post->created_at != $post->updated_at)
                                    <span class="text-muted"
                                        style="font-size: .7rem">
                                        Edited
                                        {{$post->updated_at->diffForHumans()}}
                                    </span>
                                    <i class="fa-solid fa-pen"></i>
                                @endif
                                <br>
                                <span class="text-muted"
                                    style="font-size: .7rem">
                                    {{$post->created_at->diffForHumans()}}
                                </span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col col-md">
                                <p>
                                    {{$post->content}}
                                    <a style="float: right"
                                        href="{{route('post.show', [$post->user->username, $post])}}">
                                        See post
                                        <i class="fa-solid fa-angles-right"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md" style="text-align: center">
                                <img class="img-fluid"
                                    style="height: 300px"
                                    src="{{$post->image}}"
                                    alt="">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-9"
                                style="text-align: left">
                                <span id="likesCountSpan_{{$post->id}}"
                                    class="text-muted"
                                    style="font-size: .7rem; padding: 30px">
                                    {{$post->likes->count()}}
                                    {{Str::plural('Like', $post->likes->count())}}
                                </span>
                                <span class="text-muted"
                                    style="font-size: .7rem; padding: 30px">
                                    {{$post->comments->count()}}
                                    {{Str::plural('Comment', $post->comments->count())}}
                                </span>
                                <span class="text-muted"
                                    style="font-size: .7rem; padding: 30px">
                                    {{$post->sharedByUsers->count()}}
                                    {{Str::plural('Share', $post->sharedByUsers->count())}}
                                </span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-4"
                                style="text-align: center">
                                <button type="button"
                                    id="likeUnlikeBtn_{{$post->id}}"
                                    name="likeUnlikeBtn_{{$post->id}}"
                                    onclick="{{$post->likedByUser($user) ? 'unlikePost' : 'likePost' }}({{$post->id}})"
                                    class="btn btn-light"
                                    style="text-align: center; width: 100%">
                                    <i class="fa-solid fa-thumbs-up"
                                        id="likeUnlikeicon_{{$post->id}}"></i>
                                    {{$post->likedByUser($user) ? 'Unlike' : 'Like' }}
                                </button>
                            </div>
                            <div class="col-4"
                                style="text-align: center">
                                <button type="button"
                                    class="btn btn-light"
                                    style="text-align: center; width: 100%"
                                    data-bs-toggle="collapse"
                                    data-bs-target="#commentBody_{{$post->id}}"
                                    aria-expanded="false"
                                    aria-controls="commentBody_{{$post->id}}">
                                    Comment
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col">
                                <div class="collapse"
                                    id="commentBody_{{$post->id}}">
                                    @include('pages.comment.create')
                                    @include('pages.comment.index')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="text-muted"
                style="text-align: center">
                <div class="card">
                    <div class="card-body">
                        <p>
                            <i class="fa-solid fa-circle-exclamation"></i> Content is not available!
                            <br>
                            This Post was removed by the owner.
                        </p>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>