<!-- Edit Post Modal -->
<div class="modal fade"
    id="editPostModal"
    tabindex="-1"
    aria-labelledby="editPostModal"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Post</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                @csrf
                <div class="container">
                    <div class="row">
                        <div class="col-md">
                            <input type="hidden"
                                name="postID"
                                id="postID">
                            <input type="hidden"
                                name="originalImagePath"
                                id="originalImagePath">
                            <textarea name="postContent" 
                                id="postContent"
                                class="form-control"
                                style="width: 100%; height: 200px"></textarea>

                            <span id="postContentErrorSpan"
                                class="invalid-feedback"
                                role="alert">
                            </span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <form method="POST"
                                id="newImageUploadForm"
                                enctype="multipart/form-data">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-9 col-md-9">
                                            <input class="form-control"
                                                type="file"
                                                name="newImage"
                                                id="newImage">

                                            <span id="newImageErrorSpan"
                                                class="invalid-feedback"
                                                role="alert">
                                            </span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <button type="button"
                                                id="newImageUploadBtn"
                                                onclick="newPostImageUpload()"
                                                style="width: 100px"
                                                class="btn btn-outline-secondary">
                                                Upload
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md"
                            style="text-align: center">
                            <img class="img-fluid"
                                id="postImagePrev"
                                style="height: 200px;"
                                src=""
                                alt="">

                            <span id="postImagePrevErrorSpan"
                                class="invalid-feedback"
                                role="alert">
                            </span>

                            <br>
                            <br>
                            <button id="postRemoveImageBtn"
                                hidden
                                onclick="postRemoveImage()" 
                                class="btn btn-danger">
                                Remove
                            </button>
                        </div>
                    </div>
                </div>
                <br>
                <div id="updatePostDiv"
                    style="text-align: right">
                    <button type="submit"
                        id="updatePostBtn"
                        onclick="updatePost()"
                        class="btn btn-success">
                        Update
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
