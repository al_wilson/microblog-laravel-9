<!-- Delete Post Modal -->
<div class="modal fade"
    id="deletePostModal"
    tabindex="-1"
    aria-labelledby="deletePostModal"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                @csrf
                <input type="hidden"
                    id="deletePostID"
                    name="deletePostID">
                <div style="text-align: center">
                    <i class="fa-solid fa-4x fa-circle-exclamation" style="color: red"></i>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col"
                            style="text-align: center">
                            <p id="deletePostModalMessage">
                            </p>
                        </div>
                    </div>
                </div>
                <br>
                <div id="deletePostModalBtnDiv"
                    style="text-align: center">
                    <button type="submit"
                        id="deletePostBtn"
                        onclick="deletePost()"
                        class="btn btn-danger">
                        Delete
                    </button>
                    <button type="button"
                        class="btn btn-primary"
                        data-bs-dismiss="modal">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
