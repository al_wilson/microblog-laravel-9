@extends('layouts.app')

@section('content')

<!-- for post -->
@include('partials.flashMessages')
@include('pages.post.createPost')
@include('pages.post.editPost')
@include('pages.post.deletePost')
@include('pages.post.indexPost')
@include('pages.post.sharePost')
<!-- for comment -->
@include('pages.comment.delete')

@endsection

@section('js')

<!-- js file for post -->
<script src="{{mix('js/post/createPost.js')}}"></script>
<script src="{{mix('js/post/editPost.js')}}"></script>
<script src="{{mix('js/post/deletePost.js')}}"></script>
<script src="{{mix('js/post/sharePost.js')}}"></script>
<script src="{{mix('js/post/editSharedPost.js')}}"></script>
<script src="{{mix('js/post/deleteSharedPost.js')}}"></script>
<!-- js file for comment -->
<script src="{{mix('js/comment/create.js')}}"></script>
<script src="{{mix('js/comment/edit.js')}}"></script>
<script src="{{mix('js/comment/delete.js')}}"></script>
<!-- js file for like and unlike -->
<script src="{{mix('js/like.js')}}"></script>
<!-- js file for follow and unfollow -->
<script src="{{mix('js/follow.js')}}"></script>

@endsection