<!-- Delete comment Modal -->
<div class="modal fade"
    id="deleteCommentModal"
    tabindex="-1"
    aria-labelledby="deleteCommentModal"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                @csrf
                <input type="hidden"
                    id="deleteCommentID"
                    name="deleteCommentID">
                <div style="text-align: center">
                    <i class="fa-solid fa-4x fa-circle-exclamation" style="color: red"></i>
                </div>
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col"
                            style="text-align: center">
                            <p>
                                Are you sure you want to delete this comment?
                                <br>
                                This action cannot be undone.
                            </p>
                        </div>
                    </div>
                </div>
                <br>
                <div id="deleteCommentModalBtnDiv"
                    style="text-align: center">
                    <button type="submit"
                        id="deleteCommentBtn"
                        onclick="deleteComment()"
                        class="btn btn-danger">
                        Delete
                    </button>
                    <button type="button"
                        class="btn btn-primary"
                        data-bs-dismiss="modal">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
