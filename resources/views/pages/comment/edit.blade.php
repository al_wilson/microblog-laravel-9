<!-- Edit Comment Collapse Body -->
<br>
<div class="card card-body">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-2">
                <div class="user-profile-img-div">
                    <img class="img-fluid img-thumbnail user-profile-img"
                    src="{{$comment->user->image ?? asset('uploads/user_images/default-user-image.png')}}">
                </div>
            </div>
            <div class="col-9">
                <textarea class="form-control"
                    rows="1"
                    id="editComment_{{$comment->id}}"
                    name="editComment_{{$comment->id}}">{{$comment->comment}}</textarea>

                <span id="editCommentErrorSpan_{{$comment->id}}"
                    class="invalid-feedback"
                    role="alert">
                </span>
            </div>
            <div class="col-1">
                <button type="button"
                    class="btn btn-primary"
                    id="editCommentBtn_{{$comment->id}}"
                    name="editCommentBtn_{{$comment->id}}"
                    onclick="updateComment({{$comment->id}})">
                    <i id="editCommentSendIcon_{{$comment->id}}"
                        class="fa-solid fa-paper-plane">
                    </i>
                </button>
            </div>
        </div>
    </div>
</div>
