<!-- Create Comment Collapse Body -->
<div class="card card-body">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-2">
                <div class="user-profile-img-div">
                    <img class="img-fluid img-thumbnail user-profile-img"
                    src="{{Auth::user()->image ?? asset('uploads/user_images/default-user-image.png')}}">
                </div>
            </div>
            <div class="col-9">
                <textarea class="form-control"
                    rows="1"
                    id="comment_{{$post->id}}"
                    name="comment_{{$post->id}}"
                    placeholder="comment..."></textarea>

                <span id="commentErrorSpan_{{$post->id}}"
                    class="invalid-feedback"
                    role="alert">
                </span>
            </div>
            <div class="col-1">
                <button type="button"
                    class="btn btn-primary"
                    id="commentBtn_{{$post->id}}"
                    name="commentBtn_{{$post->id}}"
                    onclick="storeComment({{$post->id}})">
                    <i id="commentSendIcon_{{$post->id}}"
                        class="fa-solid fa-paper-plane">
                    </i>
                </button>
            </div>
        </div>
    </div>
</div>
