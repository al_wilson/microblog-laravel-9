@if(count($post->comments) > 0)
    @foreach ($post->comments as $key => $comment)
        @if (Route::currentRouteName() == 'home')
            @if ($key == 3)
                @break
            @endif
        @endif
        <br>
        <div class="card card-body">
            <div class="container">
                <div class="row">
                    <div class="col-2">
                        <div class="user-profile-img-div">
                            <img class="img-fluid img-thumbnail user-profile-img"
                            src="{{$comment->user->image ?? asset('uploads/user_images/default-user-image.png')}}">
                        </div>
                    </div>
                    <div class="col-9">
                        <a href="" >
                            {{$comment->user->first_name}}
                            {{$comment->user->last_name}}
                            {{$comment->user->middle_name}}
                        </a>
                        @if ($comment->created_at != $comment->updated_at)
                            <span class="text-muted"
                                style="font-size: .7rem">
                                Edited
                                {{$comment->updated_at->diffForHumans()}}
                            </span>
                            <i class="fa-solid fa-pen"></i>
                        @endif
                        <br>
                        <span class="text-muted"
                            style="font-size: .7rem">
                            Commented
                        </span>
                        <span class="text-muted"
                            style="font-size: .7rem">
                            {{$comment->created_at->diffForHumans()}}
                        </span>
                        <br>
                        <p>
                            {{$comment->comment}}
                        </p>
                        @if($comment->user->id == Auth::user()->id)
                            <button type="button"
                                class="btn btn-light"
                                data-bs-toggle="collapse"
                                data-bs-target="#editCommentBody_{{$comment->id}}"
                                aria-expanded="false"
                                aria-controls="editCommentBody_{{$comment->id}}">
                                <span class="text-muted"
                                    style="font-size: .7rem">
                                    Edit
                                </span>
                            </button>
                            <button type="button"
                                class="btn btn-light"
                                onclick="deleteCommentModal({{$comment->id}})">
                                <span class="text-muted"
                                    style="font-size: .7rem">
                                    Delete
                                </span>
                            </button>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="collapse"
                            id="editCommentBody_{{$comment->id}}">
                            @include('pages.comment.edit')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <br>
    @if (Route::currentRouteName() == 'post.show')
        {{ $post->comments->links() }}
    @else
        <a href="{{route('post.show', [$post->user->username, $post])}}">View all comments <i class="fa-solid fa-angles-right"></i></a>
    @endif
@endif
