<div class="container">
    @if (session('success'))
        <div id="flashMessage" class="alert alert-success">
            {{session('success')}}
        </div>
    @elseif (session('warning'))
        <div id="flashMessage" class="alert alert-warning">
            {{session('warning')}}
        </div>
    @endif
</div>
