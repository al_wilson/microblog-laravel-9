@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="text-align: center">
                {{-- <div class="card-header">{{ __('Verify Your Email Address') }}</div> --}}
                <i class="bi bi-cloud-check"></i>
                

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @else
                        {{ __('Registration Successfull.') }}
                        <br>
                    @endif
                    {{ __('Please check your email for verification to activate your account.') }}
                    <br>
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                    <br><br>
                    <form action="{{route('logout')}}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-outline-secondary" style="width: 100px">OK</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
