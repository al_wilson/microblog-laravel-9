likePost = function (post_id) {
    $.ajax({
        type: "POST",
        url: "/post/" + post_id + "/like",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val()
        },
        beforeSend: function () {
            $('#likeUnlikeBtn_' + post_id).prop('disabled', true);
            $('#likeUnlikeicon_' + post_id).attr('class', 'fa-solid fa-spinner fa-spin');
        },
        complete: function () {
            $('#likeUnlikeBtn_' + post_id).prop('disabled', false);
        },
        success: function (response) {
            if (response.status === 'like') {
                $('#likeUnlikeBtn_' + post_id).attr('onClick', 'unlikePost(' + post_id + ')');
                $('#likeUnlikeBtn_' + post_id).html(
                    '<i class="fa-solid fa-thumbs-up" '
                    + 'id="likeUnlikeicon_' + post_id + '"></i> Unlike'
                );
                $('#likesCountSpan_' + post_id).text(response.likesCount)
            }
        }
    });
}

unlikePost = function (post_id) {
    $.ajax({
        type: "POST",
        url: "/post/" + post_id + "/unlike",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val()
        },
        beforeSend: function () {
            $('#likeUnlikeBtn_' + post_id).prop('disabled', true);
            $('#likeUnlikeicon_' + post_id).attr('class', 'fa-solid fa-spinner fa-spin');
        },
        complete: function () {
            $('#likeUnlikeBtn_' + post_id).prop('disabled', false);
        },
        success: function (response) {
            if (response.status === 'unlike') {
                $('#likeUnlikeBtn_' + post_id).attr('onClick', 'likePost(' + post_id + ')');
                $('#likeUnlikeBtn_' + post_id).html(
                    '<i class="fa-solid fa-thumbs-up" '
                    + 'id="likeUnlikeicon_' + post_id + '"></i> Like'
                );
                $('#likesCountSpan_' + post_id).text(response.likesCount)
            }
        }
    });
}