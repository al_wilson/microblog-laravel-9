sharePostModal = function (post_id){
    $('#sharePostModal').modal('show')
    $.ajax({
        type: "GET",
        url: "/post/" + post_id + "/edit",
        dataType: "json",
        success: function (response) {
            $('#DeletedSharesPostMessage').remove();
            $('#postToShareContainer').show();
            $('#postToShareCardBody').attr('style', '');
            let userFullName = response.post.user.first_name + ' '
                + (response.post.user.middle_name == null ? ' ' : response.post.user.middle_name) + ' '
                + response.post.user.last_name
            let userImage = (response.post.user.image == null ? '/uploads/user_images/default-user-image.png' : response.post.user.image)
            $('#sharePostId').val(response.post.id)
            $('#sharePostUserImage').attr('src', userImage);
            $('#sharePostUsername').text(userFullName);
            $('#sharePostTimestamp').text(response.post.new_created_at);
            $('#sharePostContent').text(response.post.content);
            $('#sharePostImage').attr('src', response.post.image)
            if (response.post.updated_at != response.post.created_at) {
                $('#sharePostUsername').append(
                    ' <span class="text-muted"'
                    + 'style="font-size: .7rem">'
                    + 'Edited '
                    + response.post.new_updated_at
                    + '</span> '
                    + '<i class="fa-solid fa-pen"></i>'
                );                      
            }
            $('textarea#sharePost_content').val('')
            $('#sharePostBtn').text('Share')
            $('#sharePostBtn').attr('onClick', 'sharePost()')
        },
        error: function (response) {
        }
    });
}

sharePost = function () {
    $.ajax({
        type: "POST",
        url: "/sharedPost",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            post_id: $('#sharePostId').val(),
            sharePost_content: $('#sharePost_content').val()
        },
        beforeSend: function () {
            $('#sharePostBtn').prop('disabled', true)
            $('#sharePostBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> '
                + 'Share'
            );
        },
        success: function (response) {
            window.location.href = response.route;
        },
        error: function (response) {
            $('#sharePostBtn').prop('disabled', false)
            $('#sharePostBtn').html('Share');
            $('#sharePost_content').attr('class', 'form-control is-invalid')
            $('#sharePost_contentErrorSpan').html(
                '<stron>' + response.responseJSON.errors.sharePost_content + '</strong>'
            );
        }
    });
}

$(document).ready(function () {
    $('#sharePostModal').on('hidden.bs.modal', function () {
        $('#sharePost_content').val('');
        $('#sharePost_content').attr('class','form-control');
        $('#sharePost_contentErrorSpan').html('');
    })
});