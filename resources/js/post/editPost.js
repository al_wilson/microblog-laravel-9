editPostModal = function (post_id) {
    $('#editPostModal').modal('show');
    
    $.ajax({
        type: "GET",
        url: "/post/" + post_id + "/edit",
        dataType: "json",
        success: function (response) {
            $('#postID').val(response.post.id);
            $('#postContent').val(response.post.content);
            $('#postImagePrev').attr('src', response.post.image);
            $('#originalImagePath').val(response.post.image);
        }
    });
}

newPostImageUpload = function () {
    var formData = new FormData();
    var imageFile = $('#newImage')[0].files;

    if (imageFile.length > 0) {
        formData.append('image', imageFile[0])
        formData.append('_token', $('input[name=_token]').val())

        $.ajax({
            type: "POST",
            url: "/post/uploadPostImage",
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function (){
                $('#newImageUploadBtn').prop('disabled', true)
                $('#newImageUploadBtn').html(
                    '<i class="fa-solid fa-spinner fa-spin"></i> Upload'
                );
            },
            success: function (response) {
                $('#newImage').attr("class","form-control")
                $('#newImage').prop('disabled', true)
                $('#newImageErrorSpan').html('')
                $('#postImagePrev').attr('src', response.filePath)
                $('#newImageUploadBtn').prop('disabled', true)
                $('#newImageUploadBtn').html('Upload');
                $('#postRemoveImageBtn').attr('hidden', false)
            },
            error: function (response) {
                $('#newImageUploadBtn').prop('disabled', false)
                $('#newImageUploadBtn').html('Upload');
                $('#newImage').attr("class","form-control is-invalid")
                $('#newImageErrorSpan').html(
                    '<strong>' + response.responseJSON.errors['image'] + '</strong>'
                );
            }
        });
    } else if (imageFile.length == 0) {
        $('#newImage').attr("class","form-control is-invalid")
        $('#newImageErrorSpan').html(
            '<strong>Please Select an image to upload.</strong>'
        );
    }
}

postRemoveImage = function () {
    $.ajax({
        type: "POST",
        url: "/post/removePostImage",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            filePath: $('#postImagePrev').attr('src')
        },
        success: function (response) {
            $('#postImagePrev').attr('src', $('#originalImagePath').val());
            $('#newImage').prop('disabled', false)
            $('#newImageUploadBtn').prop('disabled', false)
            $('#postRemoveImageBtn').attr('hidden', true);
        },
        error: function (response) {
            $('#postImagePrevErrorSpan').html(
                '<strong>Something went wrong please reload the page.</strong>'
            );
        }
    });
}

updatePost = function () {
    var post_id = $('#postID').val();
    $.ajax({
        type: "POST",
        url: "/post/" + post_id,
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            _method: "PUT",
            id: post_id,
            content: $('#postContent').val(),
            imagePath: $('#postImagePrev').attr('src')
        },
        beforeSend: function(){
            $('#updatePostBtn').prop('disabled', true);
			$('#updatePostBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> Update'
            );
		},
		complete: function(){
            $('#updatePostBtn').text('Update');
            $('#updatePostBtn').prop('disabled', false);
		},
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            if (response.responseJSON.errors) {
                $('#postContent').attr("class","form-control is-invalid")
                $('#postContentErrorSpan').html(
                    '<strong>' + response.responseJSON.errors['content'] +'</strong>'
                );
            } else {
                $('#updatePostDivErrorSpan').remove();
                $('#updatePostDiv').prepend(
                    '<span id="updatePostDivErrorSpan" style="color:red">'
                    + '<strong>*' + response.responseJSON.message +'</strong>'
                    + '</span>'
                );
            }
        }
    });
}

$(document).ready(function () {
    $('#editPostModal').on('hidden.bs.modal', function () {
        $('#postID').val('');
        $('#originalImagePath').val('');

        $('#postContent').val('');
        $('#postContent').attr('class','form-control');
        $('#postContentErrorSpan').html('');
        
        $('#newImage').val('');
        $('#newImage').attr('class', 'form-control');
        $('#newImage').prop('disabled', false)
        $('#newImageErrorSpan').html('');
        
        $('#newImageUploadBtn').prop('disabled', false);
        
        $('#postImagePrev').attr('src', '');
        $('#postImagePrevErrorSpan').html('');

        $('#postRemoveImageBtn').attr('hidden', true);
        $('#updatePostDivErrorSpan').remove();
    })
});