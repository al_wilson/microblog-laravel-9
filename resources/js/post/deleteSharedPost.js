deleteSharedPostModal = function (sharedPost_id) {
    $('#deletePostModal').modal('show');
    $('#deletePostID').val(sharedPost_id);
    $('#deletePostModalMessage').empty()
    $('#deletePostModalMessage').append(
        'Are you sure you want to delete this Shared post?'
        + '<br>'
        + 'This action cannot be undone.'
    );
    $('#deletePostBtn').attr('onClick', 'deleteSharedPost()')
}

deleteSharedPost = function () {
    $.ajax({
        type: "POST",
        url: "/sharedPost/" + $('#deletePostID').val(),
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            _method: "DELETE"
        },
        beforeSend: function () {
            $('#deletePostBtn').prop('disabled', true);
			$('#deletePostBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> Delete'
            );
        },
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            $('#deletePostBtn').prop('disabled', false);
			$('#deletePostBtn').html('Delete');
            $('#deletePostModalErrorSpan').remove();
            $('#deletePostModalBtnDiv').append(
                '<span id="deletePostModalErrorSpan" style="color:red">'
                + '<br>'
                + '<strong>*' + response.responseJSON.message + '</strong>'
                + '</span>'
            );
        }
    });
}