uploadPostImage = function () {
    var formData = new FormData();
    var imageFile = $('#image')[0].files;

    if (imageFile.length > 0 && imageFile[0].size < 10000000) {
        formData.append('image', imageFile[0])
        formData.append('_token', $('input[name=_token]').val())

        $.ajax({
            type: "POST",
            url: "/post/uploadPostImage",
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function (){
                $('#uploadImageBtn').prop('disabled', true)
                $('#uploadImageBtn').html(
                    '<i class="fa-solid fa-spinner fa-spin"></i> Upload'
                );
            },
            success: function (response) {
                $('#image').attr('class','form-control')
                $('#image').prop('disabled', true)
                $('#imageErrorSpan').html('')
                $('#imagePrev').attr('src', response.filePath)
                $('#uploadImageBtn').prop('disabled', true)
                $('#uploadImageBtn').html('Upload');
                $('#removeImageBtn').attr('hidden', false)
                $('#uploadImageBtn').html('Upload');
            },
            error: function (response) {
                $('#uploadImageBtn').prop('disabled', false)
                $('#uploadImageBtn').html('Upload');
                $('#image').attr('class','form-control is-invalid')
                $('#imageErrorSpan').html(
                    '<strong>' + response.responseJSON.errors['image'] + '</strong>'
                );
            }
        });
    } else if (imageFile.length == 0) {
        $('#image').attr('class','form-control is-invalid')
        $('#imageErrorSpan').html(
            '<strong>Please Select an image to upload.</strong>'
        );
    } else if (imageFile[0].size > 10000000) {
        $('#image').attr('class','form-control is-invalid')
        $('#imageErrorSpan').html(
            '<strong>The image must not be greater than 5000 kilobytes.</strong>'
        );
    }
}

removeImage = function () {
    $.ajax({
        type: "POST",
        url: "/post/removePostImage",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            filePath: $('#imagePrev').attr('src')
        },
        beforeSend: function (){
            $('#removeImageBtn').prop('disabled', true)
            $('#removeImageBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> Remove'
            );
        },
        success: function (response) {
            $('#imagePrev').attr('src', '');
            $('#image').prop('disabled', false)
            $('#uploadImageBtn').prop('disabled', false)
            $('#removeImageBtn').attr('hidden', true);
            $('#removeImageBtn').prop('disabled', false)
            $('#removeImageBtn').html('Remove');
        },
        error: function (response) {
            $('#removeImageBtn').prop('disabled', false)
            $('#imagePrevErrorSpan').html(
                '<strong>Something went wrong please reload the page.</strong>'
            );
        }
    });
}

storePost = function () {
    $.ajax({
        type: "POST",
        url: "/post",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            content: $('#content').val(),
            imagePath: $('#imagePrev').attr('src')
        },
        beforeSend: function(){
            $('#storePostBtn').prop('disabled', true);
			$('#storePostBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> Post'
            );
		},
		complete: function(){
            $('#storePostBtn').text('Post');
		},
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            $('#storePostBtn').prop('disabled', false);
            $('#content').attr('class','form-control is-invalid')
            $('#contentErrorSpan').html(
                '<strong>' + response.responseJSON.errors['content'] +'</strong>'
            );
        }
    });
}

$(document).ready(function () {
    $('#createPostModal').on('hidden.bs.modal', function () {
        $('#content').val('');
        $('#content').attr('class','form-control');
        $('#contentErrorSpan').html('');
        
        $('#image').val('');
        $('#image').attr('class', 'form-control');
        $('#image').prop('disabled', false)
        $('#imageErrorSpan').html('');
        
        $('#uploadImageBtn').prop('disabled', false);
        
        $('#imagePrev').attr('src', '');
        $('#imagePrevErrorSpan').html('');

        $('#removeImageBtn').attr('hidden', true);
    })
});