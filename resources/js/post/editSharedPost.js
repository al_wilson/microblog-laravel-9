sharedPostModal = function (sharedPost_id) {
    $('#sharePostModal').modal('show');
    $.ajax({
        type: "GET",
        url: "/sharedPost/" + sharedPost_id + "/edit",
        dataType: "json",
        success: function (response) {
            $('#DeletedSharesPostMessage').remove();
            $('#postToShareContainer').show();
            $('#postToShareCardBody').attr('style', '');
            $('#sharedPost_id').val(response.sharedPost.id);
            $('textarea#sharePost_content').val(response.sharedPost.content)
            if (response.sharedPost.post.deleted_at == null) {
                let userFullName = response.sharedPost.post.user.first_name
                    + ' '
                    + (response.sharedPost.post.user.middle_name == null ? ' ' : response.sharedPost.post.user.middle_name)
                    + ' '
                    + response.sharedPost.post.user.last_name
                let userImage = (response.sharedPost.post.user.image == null ? '/uploads/user_images/default-user-image.png' : response.sharedPost.post.user.image)
                $('#sharePostUserImage').attr('src', userImage);
                $('#sharePostUsername').text(userFullName);
                $('#sharePostTimestamp').text(response.sharedPost.post.new_created_at);
                if (response.sharedPost.post.updated_at != response.sharedPost.post.created_at) {
                    $('#sharePostUsername').append(
                        ' <span class="text-muted"'
                        + 'style="font-size: .7rem">'
                        + 'Edited '
                        + response.sharedPost.post.new_updated_at
                        + '</span> '
                        + '<i class="fa-solid fa-pen"></i>'
                    );                      
                }
                $('#sharePostContent').text(response.sharedPost.post.content);
                $('#sharePostImage').attr('src', response.sharedPost.post.image);
            } else {
                $('#DeletedSharesPostMessage').remove();
                $('#postToShareContainer').hide();
                $('#postToShareCardBody').attr('style', 'text-align: center');
                $('#postToShareCardBody').append(
                    '<p class="text-muted" id="DeletedSharesPostMessage">'
                    + '<i class="fa-solid fa-circle-exclamation"></i> Content is not available!'
                    + '<br>'
                    + 'This Post was removed by the owner.'
                    + '</p>'
                );
            }
            $('#sharePostBtn').text('Update')
            $('#sharePostBtn').attr('onClick', 'updateSharedPost()')
        }
    });
}

updateSharedPost = function () {
    $.ajax({
        type: "POST",
        url: "/sharedPost/" + $('#sharedPost_id').val(),
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            _method: "PUT",
            sharePost_content: $('#sharePost_content').val()
        },
        beforeSend: function () {
            $('#sharePostBtn').prop('disabled', true)
            $('#sharePostBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> '
                + 'Update'
            );
        },
        success: function (response) {
            window.location.href = response.route;
        },
        error: function (response) {
            $('#sharePostBtn').prop('disabled', false)
            $('#sharePostBtn').html('Update');
            if (response.responseJSON.errors) {
                $('#sharePost_content').attr('class', 'form-control is-invalid')
                $('#sharePost_contentErrorSpan').html(
                    '<stron>' + response.responseJSON.errors.sharePost_content + '</strong>'
                );
            } else {
                $('#sharePostDivErrorSpan').remove();
                $('#sharePostBtnDiv').prepend(
                    '<span id="sharePostDivErrorSpan" style="color:red">'
                    + '<strong>*' + response.responseJSON.message +'</strong>'
                    + '</span>'
                );
            }
        }
    });
}