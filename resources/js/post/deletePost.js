deletePostModal = function (post_id) {
    $('#deletePostModal').modal('show');
    $('#deletePostID').val(post_id);
    $('#deletePostModalMessage').empty()
    $('#deletePostModalMessage').append(
        'Are you sure you want to delete this post?'
        + '<br>'
        + 'This action cannot be undone.'
    );
    $('#deletePostBtn').attr('onClick', 'deletePost()')
}

deletePost = function () {
    var post_id = $('#deletePostID').val();
    $.ajax({
        type: "POST",
        url: "/post/" + post_id,
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            _method: "DELETE",
            id: post_id
        },
        beforeSend: function () {
            $('#deletePostBtn').prop('disabled', true);
			$('#deletePostBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> Delete'
            );
        },
        complete: function () {
            $('#deletePostBtn').prop('disabled', false);
            $('#deletePostBtn').text('Delete');
        },
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            $('#deletePostModalErrorSpan').remove();
            $('#deletePostModalBtnDiv').append(
                '<span id="deletePostModalErrorSpan" style="color:red">'
                + '<br>'
                + '<strong>*' + response.responseJSON.message + '</strong>'
                + '</span>'
            );
        }
    });
}