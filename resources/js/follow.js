follow = function (username) {
    $.ajax({
        type: "POST",
        url: "/profile/" + username + "/follow",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            url: window.location.href
        },
        beforeSend: function () {
            $('#followerBtn_' + username).prop('disabled', true);
            $('#followerBtn_' + username).prepend(
                '<i class="fa-solid fa-spinner fa-spin"></i> '
            );
            $('#followingBtn_' + username).prop('disabled', true);
            $('#followingBtn_' + username).prepend(
                '<i class="fa-solid fa-spinner fa-spin"></i> '
            );
        },
        complete: function () {
            $('#followerBtn_' + username).prop('disabled', false);
            $('#followingBtn_' + username).prop('disabled', false);
        },
        success: function (response) {
            if (response.status === 'follow') {
                $('#followerBtn_' + username).attr('onClick', 'unfollow("' + username + '")');
                $('#followerBtn_' + username).html('Unfollow');
                $('#followingBtn_' + username).attr('onClick', 'unfollow("' + username + '")');
                $('#followingBtn_' + username).html('Unfollow');
                if (response.url === location.href) {
                    $('#userFollowingCount').text(response.followingCount);
                }
            } else if (response.route) {
                location.href = response.route;
            }
        }
    });
}

unfollow = function (username) {
    $.ajax({
        type: "POST",
        url: "/profile/" + username + "/unfollow",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            url: window.location.href
        },
        beforeSend: function () {
            $('#followerBtn_' + username).prop('disabled', true);
            $('#followerBtn_' + username).prepend(
                '<i class="fa-solid fa-spinner fa-spin"></i> '
            );
            $('#followingBtn_' + username).prop('disabled', true);
            $('#followingBtn_' + username).prepend(
                '<i class="fa-solid fa-spinner fa-spin"></i> '
            );
        },
        complete: function () {
            $('#followerBtn_' + username).prop('disabled', false);
            $('#followingBtn_' + username).prop('disabled', false);
        },
        success: function (response) {
            if (response.status === 'unfollow') {
                $('#followerBtn_' + username).attr('onClick', 'follow("' + username + '")');
                $('#followerBtn_' + username).html('Follow');
                $('#followingBtn_' + username).attr('onClick', 'follow("' + username + '")');
                $('#followingBtn_' + username).html('Follow');
                if (response.url === location.href) {
                    $('#userFollowingCount').text(response.followingCount);
                }
            } else if (response.route) {
                location.href = response.route;
            }
        }
    });
}
