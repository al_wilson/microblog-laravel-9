uploadUserImage = function () {
    let formData = new FormData();
    let imageFile = $('#userImage')[0].files;
    let userName = $('#originalUsername').val();

    if (imageFile.length > 0 && imageFile[0].size < 10000000) {
        formData.append('image', imageFile[0])
        formData.append('_token', $('input[name=_token]').val())

        $.ajax({
            type: "POST",
            url: "/profile/" + userName + "/uploadUserImage",
            dataType: "json",
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function () {
                $('#userImage').prop('disabled', true);
                $('#uploadUserImageBtn').prop('disabled', true);
                $('#uploadUserImageBtn').html(
                    '<i class="fa-solid fa-spin fa-spinner"></i> '
                    + 'Upload'
                )
            },
            complete: function () {
                $('#uploadUserImageBtn').html('Upload')
            },
            success: function (response) {
                $('#userImage').attr('class','form-control')
                $('#userImage').prop('disabled', true)
                $('#userImageErrorSpan').html('')
                $('#userImagePrev').attr('src', response.filePath)
                $('#uploadUserImageBtn').prop('disabled', true)
                $('#userImageIcon').hide();
                $('#removeUserImageDiv').append(
                    '<button '
                    + 'id="removeUserImageBtn" '
                    + 'onclick="removeUserImage()" '
                    + 'class="btn btn-danger"> '
                    + 'Remove '
                    + '</button> '
                )
            },
            error: function (response) {
                $('#userImage').prop('disabled', false);
                $('#uploadUserImageBtn').prop('disabled', false);
                $('#uploadUserImageBtn').html('Upload');
                $('#userImage').attr('class','form-control is-invalid');
                $('#userImageErrorSpan').html(
                    '<strong>' + response.responseJSON.errors['image'] + '</strong>'
                );
            }
        });
    } else if (imageFile.length == 0) {
        $('#userImage').attr('class','form-control is-invalid')
        $('#userImageErrorSpan').html(
            '<strong>Please Select an image to upload.</strong>'
        );
    } else if (imageFile[0].size > 10000000) {
        $('#userImage').attr('class','form-control is-invalid')
        $('#userImageErrorSpan').html(
            '<strong>The image must not be greater than 5000 kilobytes.</strong>'
        );
    } 
}

removeUserImage = function () {
    $.ajax({
        type: "POST",
        url: "/profile/" + userName + "/removeUserImage",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            filePath: $('#userImagePrev').attr('src')
        },
        beforeSend: function () {
            $('#removeUserImageBtn').prop('disabled', true);
            $('#removeUserImageBtn').html(
                '<i class="fa-solid fa-spin fa-spinner"></i> '
                + 'Remove'
            )
        },
        success: function (response) {
            $('#userImagePrev').attr('src', $('#originalUserImagePath').val());
            $('#userImage').prop('disabled', false);
            $('#uploadUserImageBtn').prop('disabled', false);
            $('#removeUserImageBtn').remove();
            if ($('#originalUserImagePath').val() == '') {
                $('#userImageIcon').show();
            }
        },
        error: function (response) {
            $('#userImageErrorSpan').html(
                '<strong>Something went wrong please reload the page.</strong>'
            );
        }
    });
}

updateUserProfile = function () {
    $.ajax({
        type: "POST",
        url: location.href,
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            lastName: $('#lastName').val(),
            firstName: $('#firstName').val(),
            middleName: $('#middleName').val(),
            userName: $('#userName').val(),
            email: $('#email').val(),
            imagePath: $('#userImagePrev').attr('src'),
            currentPassword: $('#currentPassword').val(),
            newPassword: $('#newPassword').val(),
            confirmPassword: $('#confirmPassword').val()
        },
        beforeSend: function () {
            $('#updateUserProfileBtn').prop('disabled', true);
            $('#updateUserProfileBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> Save'
            );
        },
        complete: function () {
            $('#updateUserProfileBtn').text('Save');
            $('#updateUserProfileBtn').prop('disabled', false);
        },
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            $('#updateUserProfileBtn').prop('disabled', false);
            $('#updateUserProfileBtn').html('Save');
            if (response.responseJSON.errors) {
                $('span.invalid-feedback').html('');
                $('input.form-control.is-invalid').attr('class', 'form-control')
                $.each(response.responseJSON.errors, function (index, value) {
                    $('#' + index).attr('class','form-control is-invalid')
                    $('#' + index + 'ErrorSpan').html(
                        '<strong>' + value +'</strong>'
                    );
                });
            } else {
                $('#updateUserProfileBtnErrorSpan').remove();
                $('#updateUserProfileBtnDiv').append(
                    '<span id="updateUserProfileBtnErrorSpan" style="color:red"> *'
                    + response.responseJSON.message
                    + '</span>'
                );
            }
            $('#updateUserProfileBtn').prop('disabled', false);
        }
    });
}