deleteCommentModal = function (comment_id) {
    $('#deleteCommentModal').modal('show');
    $('#deleteCommentID').val(comment_id)
}

deleteComment = function () {
    var comment_id = $('#deleteCommentID').val()
    $.ajax({
        type: "POST",
        url: "/comment/" + comment_id,
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            _method: "DELETE",
            id: comment_id
        },
        beforeSend: function(){
            $('#deleteCommentBtn').prop('disabled', true);
			$('#deleteCommentBtn').html(
                '<i class="fa-solid fa-spinner fa-spin"></i> Delete'
            );
		},
		complete: function(){
            $('#deleteCommentBtn').prop('disabled', false);
			$('#deleteCommentBtn').text('Delete');
		},
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            $('#deleteCommentModalErrorSpan').remove();
            $('#deleteCommentModalBtnDiv').append(
                '<span id="deleteCommentModalErrorSpan" style="color:red">'
                + '<br>'
                + '<strong>*' + response.responseJSON.message + '</strong>'
                + '</span>'
            );
        }
    });
}