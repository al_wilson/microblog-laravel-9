storeComment = function (post_id) {
    $.ajax({
        type: "POST",
        url: "/comment",
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            post_id: post_id,
            comment: $('#comment_' + post_id).val()
        },
        beforeSend: function(){
            $('#commentBtn_' + post_id).prop('disabled', true);
			$('#commentSendIcon_' + post_id).attr('class', 'fa-solid fa-spinner fa-spin');
		},
		complete: function(){
			$('#commentSendIcon_' + post_id).attr('class', 'fa-solid fa-paper-plane');
		},
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            $('#commentBtn_' + post_id).prop('disabled', false);
            $('#comment_' + post_id).attr('class','form-control is-invalid')
            $('#commentErrorSpan_' + post_id).html(
                '<strong>' + response.responseJSON.errors['comment'] +'</strong>'
            );
        }
    });
}