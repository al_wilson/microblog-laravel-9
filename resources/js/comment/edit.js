updateComment = function (comment_id) {
    $.ajax({
        type: "POST",
        url: "/comment/" + comment_id,
        dataType: "json",
        data: {
            _token: $('input[name=_token]').val(),
            _method: "PUT",
            comment: $('#editComment_' + comment_id).val()
        },
        beforeSend: function(){
            $('#editCommentBtn_' + comment_id).prop('disabled', true);
			$('#editCommentSendIcon_' + comment_id).attr('class', 'fa-solid fa-spinner fa-spin');
		},
		complete: function(){
			$('#editCommentSendIcon_' + comment_id).attr('class', 'fa-solid fa-paper-plane');
		},
        success: function (response) {
            location.href = response.route;
        },
        error: function (response) {
            $('#editCommentBtn_' + comment_id).prop('disabled', false);
            $('#editComment_' + comment_id).attr('class','form-control is-invalid')
            $('#editCommentErrorSpan_' + comment_id).html(
                '<strong>' + response.responseJSON.errors['comment'] +'</strong>'
            );
        }
    });
}