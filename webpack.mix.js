const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

// for post
mix.js('resources/js/post/createPost.js', 'public/js/post/createPost.js');
mix.js('resources/js/post/editPost.js', 'public/js/post/editPost.js');
mix.js('resources/js/post/deletePost.js', 'public/js/post/deletePost.js');
// for comment
mix.js('resources/js/comment/create.js', 'public/js/comment/create.js');
mix.js('resources/js/comment/edit.js', 'public/js/comment/edit.js');
mix.js('resources/js/comment/delete.js', 'public/js/comment/delete.js');
// for like and unlike
mix.js('resources/js/like.js', 'public/js/like.js');
// for follow and unfollow
mix.js('resources/js/follow.js', 'public/js/follow.js');
// for updating user info
mix.js('resources/js/user/editUserInfo.js', 'public/js/user/editUserInfo.js');
// for sharing post
mix.js('resources/js/post/sharePost.js', 'public/js/post/sharePost.js');
mix.js('resources/js/post/editSharedPost.js', 'public/js/post/editSharedPost.js');
mix.js('resources/js/post/deleteSharedPost.js', 'public/js/post/deleteSharedPost.js');

